import React from 'react';
import Header from './components/Header/Header';
import Navbar from './components/Navbar/Navbar';
import { Box } from 'grommet';
import Profile from './components/Profile/Profile';
import { BrowserRouter, Route } from 'react-router-dom';
import TodoListPage from './pages/TodoListPage';
import DialogsContainer from './components/Dialogs/DialogsContainer';
import UsersContainer from './components/Users/UsersContainer';

function App() {
  return (
    <BrowserRouter>
      <Box
        width={{ max: "1240px" }}
        margin={{ vertical: "0px", horizontal: "auto" }}
      >
        <Header />
        <Box
          direction="row"
        >
          <Navbar />
          <Box
            width="100%"
            pad="medium"
          >
            <Route path="/dialogs" render={() => <DialogsContainer />} />
            <Route path="/profile" render={() => <Profile />} />
            <Route path="/users" render={() => <UsersContainer />} />
            <Route path="/todolist" render={() => <TodoListPage />}
            />
          </Box>
        </Box>
      </Box>
    </BrowserRouter>
  );
}

export default App;
