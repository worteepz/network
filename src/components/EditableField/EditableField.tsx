import React, { useState, ChangeEvent } from 'react';

type EditableFieldType = {
    title: string
    onChangeField: (updateValue: string) => void
}

function EditableField(props: EditableFieldType) {

    const [editMode, setEditMode] = useState(false);
    const [title, setTitle] = useState(props.title);

    const activateEditMode = () => setEditMode(true);
    const activateViewMode = () => {
        setEditMode(false);
        props.onChangeField(title);
    }

    const onChangeTitlehandler = (e: ChangeEvent<HTMLInputElement>) => setTitle(e.currentTarget.value);

    return (
        editMode
            ? <input onChange={onChangeTitlehandler} onBlur={activateViewMode} value={title} type="text" autoFocus/>
            : <span onDoubleClick={activateEditMode}>{props.title}</span>
    )
}

export default EditableField;