import React, { ChangeEvent, useState, KeyboardEvent } from 'react';
import { Box, Button } from 'grommet';

type PropsType = {
    whatAdd: (title: string) => void
}

function NewPostinput(props: PropsType) {

    const [postTitle, setPostTitle] = useState<string>("");

    const [error, setError] = useState<string>("");

    function onNewTitleChangeHandler(e: ChangeEvent<HTMLInputElement>) {
        setPostTitle(e.currentTarget.value);
    }

    function onKeyPressHandler(e: KeyboardEvent<HTMLInputElement>) {
        setError(""); // Когда клавиша любая нажата, очищаем ошибки
        if (e.charCode === 13) {
            addingFun();
        }
    }

    function addingFun() {
        // Сбрасываем, когда только пробелы или пустые данные в заголовке
        if (postTitle.trim() === "") {
            setError("Вы не ввели данные(");
            return;
        }
        props.whatAdd(postTitle);
        setPostTitle("");
    }

    return (
        <>
            <Box direction="row" align="center" gap="small">
                {/* 
                При срабатывании onChange, берем всю информацию объекта "e" 
                И добавляем текущее value к newTaskTitle, FLUX круговорот
            */}
                <input
                    value={postTitle}
                    onChange={onNewTitleChangeHandler}
                    onKeyPress={onKeyPressHandler}
                    className={error ? "error" : ""}
                />
                <Button type="button" onClick={addingFun} label="+" size="small" />
            </Box>
            {error && <div className="error-message">{error}</div>}
        </>
    )
}

export default NewPostinput;
