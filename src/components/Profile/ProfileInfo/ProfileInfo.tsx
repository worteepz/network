import React from 'react';
import { Box, Text, Image } from 'grommet';

type PropsType = {
    img: string
    description: string
}

function ProfileInfo(props: PropsType) {
    return (
        <Box>
            <Image fit="cover" src={props.img} />
            <Text size="small" margin={{ vertical: "small" }}>
                {props.description}
            </Text>
        </Box>
    )
}

export default ProfileInfo;