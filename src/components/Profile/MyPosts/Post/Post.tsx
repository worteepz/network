import React from 'react';
import { Box, Text } from 'grommet';
import { PostType } from '../../../../redux/store';

function Post(props: PostType) {
  return (
    <Box pad={{vertical: "1em"}} key={props.id}>
        <Text margin={{bottom: "0.5em"}}>{props.text}</Text>
        <div>❤ {props.likesCount}</div>
    </Box>
  );
}

export default Post;
