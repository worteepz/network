import { ReduxStateType, CommonActionType } from '../../../redux/store';
import { addPostActionCreator, updateNewPostTextActionCreator } from '../../../redux/profile-reducer';
import MyPosts from './MyPosts';
import { connect } from 'react-redux';

function mapStateToProps(state: ReduxStateType) {
    return {
        posts: state.profilePage.posts,
        updatePostText: state.profilePage.newPostText
    }
}

function mapDispatchToProps(dispatch: (action: CommonActionType) => void) {
    return {
        updateNewPostText: (text: string) => {
            dispatch(updateNewPostTextActionCreator(text))
        },
        addPost: (message: string) => {
            dispatch(addPostActionCreator(message));
        }
    }
}

const MyPostsContainer = connect(mapStateToProps, mapDispatchToProps)(MyPosts);

export default MyPostsContainer;
