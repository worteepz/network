import React, { ChangeEvent } from 'react';
import { Box, TextArea, Button, Text } from 'grommet';
import Post from './Post/Post';
import { PostType } from '../../../redux/store';

type PropsType = {
    updatePostText: string
    posts: Array<PostType>
    addPost: (text: string) => void
    updateNewPostText: (text: string) => void
}

function MyPosts(props: PropsType) {

    let postsElements =
        props.posts.map(p => 
            <Post
                key={p.id}
                id={p.id}
                text={p.text}
                likesCount={p.likesCount}
            />
        );

    // При изменении, вводе текста, получаем текущее значение из TextArea
    function TextAreaOnChange(e:ChangeEvent<HTMLTextAreaElement>) {
        let text = e.currentTarget.value;
        props.updateNewPostText(text);
    }

    function onAddPost (message: string) {
        props.addPost(message);
    }

    function addPostHandler() {
        // Сбрасываем, когда только пробелы или пустые данные в заголовке
        let postText = props.updatePostText;
        if (postText.trim() === "") {
            return;
        }
        onAddPost(postText);
    }

    return (
        <Box>
            <Text margin={{ bottom: "small" }}>Мои посты</Text>
            <Box>
                {/* в value передаем значение текста поста, благодаря локальному стейту управляем значением */}
                <TextArea value={props.updatePostText} onChange={TextAreaOnChange} /> 
                <Button
                    onClick={addPostHandler}
                    type="button"
                    margin={{ top: "small" }}
                    label="Добавить"
                    size="small"
                />
            </Box>
            {postsElements}
        </Box>
    );
}

export default MyPosts;
