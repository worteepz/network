import React from 'react';
import { Box } from 'grommet';
import MyPostsContainer from './MyPosts/MyPostsContainer';
import ProfileInfo from './ProfileInfo/ProfileInfo';

function Profile() {
    return (
        <Box>
            <ProfileInfo />
            <MyPostsContainer />
        </Box>
    );
}

export default Profile;
