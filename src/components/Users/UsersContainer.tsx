import { connect } from 'react-redux';
import Users from './Users';
import { ReduxStateType, CommonActionType } from '../../redux/store';
import { followAC, unfollowAC, UserType, setUsersAC } from '../../redux/users-reducer';


function mapStateToProps(state: ReduxStateType) {
    return {
        users: state.usersPage.users
    }
}
function mapDispatchToProps(dispatch: (action: CommonActionType) => void) {
    return {
        follow: (userId: string) => {
            dispatch(followAC(userId));
        },
        unfollow: (userId: string) => {
            dispatch(unfollowAC(userId));
        },
        setUsers: (users: Array<UserType>) => {
            dispatch(setUsersAC(users));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);