import React from 'react';
import { UserType } from '../../redux/users-reducer';
import { Box, Button } from 'grommet';

type PropsType = {
    unfollow: (userId: string) => void
    follow: (userId: string) => void
    users: Array<UserType>
}

function Users(props: PropsType) {

    return (
        <Box>
            {
                props.users.map(u => {
                    return (
                        <Box key={u.id}>
                            <Box>
                                <img src={u.photoUrl} alt="" />
                                {
                                    u.followed
                                        ? <Button onClick={ () => {props.follow(u.id)} } type="button" label="Подписаться" />
                                        : <Button onClick={ () => {props.unfollow(u.id)} } type="button" label="Подписаться" />
                                }
                            </Box>
                            <Box border="all">
                                <Box>{u.name} {u.surname}</Box>
                                <Box>{u.status}</Box>
                                <Box>{u.location.city}</Box>
                                <Box>{u.location.country}</Box>
                            </Box>
                        </Box>
                    )
                })
            }
        </Box>
    )
}

export default Users;