import React, { ChangeEvent } from 'react';
import { TaskType } from '../../../redux/store';
import { Box, Button } from 'grommet';
import EditableField from '../../EditableField/EditableField';

type PropsType = {
    removeTask: (id: string) => void
    changeTaskStatus: (taskId: string, isDone: boolean) => void
    changeTaskTitle: (taskId: string, newTitle: string) => void
    task: TaskType
}

function Task(props: PropsType) {

    function onRemoveHandler() { props.removeTask(props.task.id) }

    function onChangeInputHandler(e: ChangeEvent<HTMLInputElement>) {
        props.changeTaskStatus(props.task.id, e.currentTarget.checked);
    }

    const onChangeFieldHandler = (newTitle: string) => {
        props.changeTaskTitle(props.task.id, newTitle);
    }

    return (
        <Box 
            direction="row"
            align="center"
            gap="small"
            margin={{vertical: "xsmall"}}
            key={props.task.id}
            className={props.task.isDone ? "is-done" : ""}
        >
            <input
                type="checkbox"
                onChange={onChangeInputHandler}
                checked={props.task.isDone}
            />
            <EditableField title={props.task.title} onChangeField={onChangeFieldHandler} />
            <Button type="button" onClick={onRemoveHandler} label="x" size="small" />
        </Box>
    );
}

export default Task;