import React, { useState } from 'react';
import Task from './Task/Task';
import { TodoListType, FilterValueType, CommonActionType } from '../../redux/store';
import { Button, Box } from 'grommet';
import NewPostinput from '../NewPostInput/NewPostInput';
import EditableField from '../EditableField/EditableField';
import { removeTaskActionCreator, addTaskActionCreator, changeTaskStatusActionCreator, changeTaskTitleActionCreator, removeTodolistActionCreator, saveTodolistTitleActionCreator } from '../../redux/todolist-reducer';

type PropsType = {
    dispatch: (action: CommonActionType) => void
    todolist: TodoListType
}

function TodoList(props: PropsType) {

    const tasks = props.todolist.tasks;

    const [filter, setFilter] = useState<FilterValueType>(props.todolist.filter);

    function onAllClickHandler() { setFilter("all") }
    function onActiveClickHandler() { setFilter("active") }
    function onCompletedClickHandler() { setFilter("completed") }

    function removeTask(taskId: string) {
        props.dispatch(removeTaskActionCreator(taskId, props.todolist.id));
    }

    function addTask(title: string) {
        props.dispatch(addTaskActionCreator(title, props.todolist.id));
    }

    function changeTaskStatus(taskId: string, isDone: boolean) {
        props.dispatch(changeTaskStatusActionCreator(taskId, isDone, props.todolist.id));
    }

    function changeTaskTitle(taskId: string, newTitle: string) {
        props.dispatch(changeTaskTitleActionCreator(taskId, newTitle, props.todolist.id));
    }

    function onRemoveTodoListHandler() {
        props.dispatch(removeTodolistActionCreator(props.todolist.id))
    }

    let tasksForTodoList = tasks;

    if (filter === "active") {
        // Выбираем только те, где isDone равен false, задания не выполнены
        tasksForTodoList = tasks.filter(t => t.isDone === false)
    } else if (filter === "completed") {
        // Выбираем только те, где isDone равен true, задания выполнены
        tasksForTodoList = tasks.filter(t => t.isDone === true)
    }

    const FilterButtons = () => {
        return (
            <Box direction="row" gap="small">
                <Button
                    type="button"
                    label="Все"
                    className={filter === "all" ? "active-filter" : ""}
                    onClick={onAllClickHandler}
                />
                <Button
                    type="button"
                    label="Активные"
                    className={filter === "active" ? "active-filter" : ""}
                    onClick={onActiveClickHandler}
                />
                <Button
                    type="button"
                    label="Выполненные"
                    className={filter === "completed" ? "active-filter" : ""}
                    onClick={onCompletedClickHandler}
                />
            </Box>
        )
    }

    function changeTodoListHandler(newTitle: string) {
        props.dispatch(saveTodolistTitleActionCreator(newTitle, props.todolist.id));
    }

    return (
        <div>
            <Box direction="row" align="center" gap="small">
                <h3>
                    <EditableField onChangeField={changeTodoListHandler} title={props.todolist.name} />
                </h3>
                <Button type="button" onClick={onRemoveTodoListHandler} label="x" />
            </Box>
            <NewPostinput whatAdd={addTask} />
            <Box>
                {tasksForTodoList.map(t =>
                    <Task
                        changeTaskTitle={changeTaskTitle}
                        changeTaskStatus={changeTaskStatus}
                        removeTask={removeTask}
                        key={t.id}
                        task={t}
                    />
                )}
            </Box>
            <FilterButtons />
        </div>
    );
}

export default TodoList;