import React from 'react';
import { Box } from 'grommet';
import { NavLink } from 'react-router-dom';
import styled from "styled-components";

const Link = styled(NavLink)`
  color: #fff;
  text-decoration: none;
`

function Navbar() {
  return (
    <Box
      pad="medium"
      background="#21201f"
      color="#fff"
      width="small"
    >
      <Link to="/profile">Мой профиль</Link>
      <Link to="/dialogs">Сообщения</Link>
      <Link to="/news">Новости</Link>
      <Link to="/music">Музыка</Link>
      <Link to="/settings">Настройки</Link>
      <Link to="/todolist">Чеклист</Link>
    </Box>
  );
}

export default Navbar;