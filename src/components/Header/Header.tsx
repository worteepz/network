import React from 'react';
import { Box } from 'grommet';

function Header() {
  return (
    <Box
      direction="row"
      pad="medium"
      background="#000"
      color="#fff"
    >
      Шапка
    </Box>
  );
}

export default Header;