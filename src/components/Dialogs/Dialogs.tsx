import React, { ChangeEvent } from 'react';
import DialogItem from './DialogItem/DialogItem';
import { Box, TextArea, Button } from 'grommet';
import Message from './Message/Message';
import { DialogPageType } from '../../redux/store';

type PropsType = {
    dialogPage: DialogPageType
    updateNewMessageBody: (body: string) => void
    addDialogMessage: (dialogMessage: string) => void
}

function Dialogs(props: PropsType) {

    let dialogMessage = props.dialogPage.body;

    function getDialogMessage(e: ChangeEvent<HTMLTextAreaElement>) {
        let body = e.currentTarget.value;
        props.updateNewMessageBody(body);
    }

    function addDialogMessageHandler() {
        // Сбрасываем, когда только пробелы или пустые данные в заголовке
        if (dialogMessage.trim() === "") {
            return;
        }
        props.addDialogMessage(dialogMessage);
    }

    return (
        <Box direction="row" border="between" gap="medium">
            <Box border="between" gap="small">
                {props.dialogPage.dialogs.map(d => <DialogItem
                    key={d.id} id={d.id} name={d.name} surname={d.surname} image={d.image} />)}
            </Box>
            <Box pad="small">
                {props.dialogPage.messages.map(m => <Message key={m.id} id={m.id} text={m.text} />)}
                <Box gap="small" margin={{ vertical: "small" }}>
                    <TextArea value={dialogMessage} onChange={getDialogMessage} />
                    <Button onClick={addDialogMessageHandler} type="button" label="Отправить" />
                </Box>
            </Box>
        </Box>
    );
}

export default Dialogs;
