import { ReduxStateType, CommonActionType } from '../../redux/store';
import { addDialogMessageActionCreator, updateNewMessageBodyActionCreator } from '../../redux/dialogs-reducer';
import Dialogs from './Dialogs';
import { connect } from 'react-redux';

function mapStateToProps(state: ReduxStateType) {
    return {
        dialogPage: state.dialogsPage
    }
}

function mapDispatchToProps(dispatch: (action: CommonActionType) => void) {
    return {
        updateNewMessageBody: (body: string) => {
            dispatch(updateNewMessageBodyActionCreator(body))
        },
        addDialogMessage: (dialogMessage: string) => {
            dispatch(addDialogMessageActionCreator(dialogMessage));
        }
    }
}

const DialogsContainer = connect(mapStateToProps, mapDispatchToProps)(Dialogs);

export default DialogsContainer;