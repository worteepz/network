import React from 'react';
import { Text } from 'grommet';
import { MessageType } from '../../../redux/store';

function Message(props: MessageType) {
    return (
        <Text key={props.id}>
            {props.text}
        </Text>
    )
}

export default Message;