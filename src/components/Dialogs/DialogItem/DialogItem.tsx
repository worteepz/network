import React from 'react';
import { Box, Avatar, Text } from 'grommet';
import { NavLink } from 'react-router-dom';
import { DialogType } from '../../../redux/store';

function DialogItem(props: DialogType) {

    let link = '/dialogs/' + props.id;

    return (
        <Box direction="row" gap="small" pad={{vertical: "small"}} align="center">
            <Avatar src={props.image} />
            <NavLink to={link}>
                <Text>{props.name} <b>{props.surname}</b></Text>
            </NavLink>
        </Box>
    )
}

export default DialogItem;