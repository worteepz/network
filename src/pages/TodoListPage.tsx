import React from 'react';
import { TodoListPageType } from '../redux/store';
import TodoList from '../components/TodoList/TodoList';
import { Box } from 'grommet';
import NewPostinput from '../components/NewPostInput/NewPostInput';
import { addTodolistActionCreator } from '../redux/todolist-reducer';

type PropsType = {
    todoListPage: TodoListPageType
}

function TodoListPage(props: PropsType) {

    function addTodoList(name: string) {
        props.dispatch(addTodolistActionCreator(name));
    }

    return (
        <Box height="100%" overflow={{ horizontal: "auto" }}>
            <NewPostinput whatAdd={addTodoList} />
            <Box direction="row" gap="small">
                {
                    props.todoListPage.todoLists.map((tl) => {
                        return (
                            <TodoList
                                dispatch={props.dispatch}
                                key={tl.id}
                                todolist={tl}
                            />
                        )
                    })
                }
            </Box>
        </Box>
    )
}

export default TodoListPage;