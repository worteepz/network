import { createStore, combineReducers } from 'redux';
import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import todolistReducer from "./todolist-reducer";
import usersReducer from './users-reducer';

const reducers = combineReducers({
    profilePage: profileReducer,
    dialogsPage: dialogsReducer,
    todolistPage: todolistReducer,
    usersPage: usersReducer
});

const store = createStore(reducers);

export type AppType = ReturnType<typeof reducers>

export default store;