import { v1 } from "uuid";
import { TodoListPageType, TodoListType, TaskType } from './store';

const initialState: TodoListPageType = {
    todoLists: [
        {
            id: v1(),
            filter: "active",
            name: "My Todolist",
            tasks: [
                { id: v1(), title: "HTML&CSS3", isDone: false },
                { id: v1(), title: "JS&JQquery", isDone: true },
                { id: v1(), title: "Java&dotNet", isDone: true }
            ]
        },
        {
            id: v1(),
            filter: "completed",
            name: "Second todolist",
            tasks: [
                { id: v1(), title: "HTML&CSS4", isDone: false },
                { id: v1(), title: "JS&JQqueris", isDone: true },
                { id: v1(), title: "Ruby&ROr", isDone: true }
            ]
        }
    ]
}

export type TodolistActionType = ReturnType<typeof addTodolistActionCreator> | ReturnType<typeof removeTodolistActionCreator> |
    ReturnType<typeof saveTodolistTitleActionCreator> | ReturnType<typeof addTaskActionCreator> | ReturnType<typeof removeTaskActionCreator> |
    ReturnType<typeof changeTaskStatusActionCreator> | ReturnType<typeof changeTaskTitleActionCreator>

const todolistReducer = (state: TodoListPageType = initialState, action: TodolistActionType): TodoListPageType => {
    switch (action.type) {
        case 'ADD-TODOLIST':
            const newTodoList: TodoListType = {
                id: v1(),
                filter: "all",
                name: action.name,
                tasks: []
            };

            return {
                ...state,
                todoLists: [...state.todoLists, newTodoList]
            }

        case 'REMOVE-TODOLIST':
            let updatedTodoLists = state.todoLists.filter((tl) =>
                tl.id !== action.todoListId);
            return {
                ...state,
                todoLists: updatedTodoLists
            };

        case 'SAVE-TODOLIST-TITLE':
            let updatedTodoList = state.todoLists.find(tl => tl.id === action.todoListId);
            if (updatedTodoList) {
                updatedTodoList.name = action.title;
                return state;
            }
            return state;

        case 'ADD-TASK':
            const newTask: TaskType = {
                id: v1(),
                title: action.title,
                isDone: false
            };
            let copyState = state.todoLists.map(el => {
                if (el.id !== action.todoListId) {
                    return el;
                } else {
                    el.tasks.push(newTask);
                    return el;
                }
            })
            return {
                ...state,
                todoLists: copyState
            };

        case 'REMOVE-TASK':
            let filteredTasks = state.todoLists.map(tl => {
                if (tl.id === action.todoListId) {
                    tl.tasks = tl.tasks.filter(t => t.id !== action.taskId);
                    return tl;
                }
                return tl;
            })
            return {
                ...state,
                todoLists: filteredTasks
            };

        case 'CHANGE-TASK-STATUS':
            let updateState = state.todoLists.map(el => {
                if (el.id === action.todoListId) {
                    let task = el.tasks.find(t => t.id === action.taskId);
                    if (task) {
                        task.isDone = action.isDone;
                    }
                    return el;
                }
                return el;
            })
            return {
                ...state,
                todoLists: updateState
            };

        case 'CHANGE-TASK-TITLE':
            let newState = state.todoLists.map(el => {
                if (el.id === action.todoListId) {
                    let task = el.tasks.find(t => t.id === action.taskId);
                    if (task) {
                        task.title = action.title;
                    }
                    return el;
                }
                return el;
            })
            return {
                ...state,
                todoLists: newState
            };

        default:
            return state;
    }
}

export const addTodolistActionCreator = (name: string) =>
    ({ type: 'ADD-TODOLIST', name } as const);

export const removeTodolistActionCreator = (todoListId: string) =>
    ({ type: 'REMOVE-TODOLIST', todoListId } as const);

export const saveTodolistTitleActionCreator = (title: string, todoListId: string) =>
    ({ type: 'SAVE-TODOLIST-TITLE', title, todoListId } as const);

export const addTaskActionCreator = (title: string, todoListId: string) =>
    ({ type: 'ADD-TASK', title, todoListId } as const);

export const removeTaskActionCreator = (taskId: string, todoListId: string) =>
    ({ type: 'REMOVE-TASK', taskId, todoListId } as const);

export const changeTaskStatusActionCreator = (taskId: string, isDone: boolean, todoListId: string) =>
    ({ type: 'CHANGE-TASK-STATUS', taskId, isDone, todoListId } as const);

export const changeTaskTitleActionCreator = (taskId: string, title: string, todoListId: string) =>
    ({ type: 'CHANGE-TASK-TITLE', taskId, title, todoListId } as const);

export default todolistReducer;