import { v1 } from "uuid";
import { ProfilePageType, PostType } from './store';

const initialState = {
    description: "Краткое описание моего профиля",
    img: "https://sun1-25.userapi.com/d2W2baMhX-1rSlU9EMmXl8728m--kaQY4WCvTw/b21rv4H-KEM.jpg",
    newPostText: 'simple post text',
    posts: [
        { id: v1(), text: 'Когда залетел в тусовку к Церебро Таргет,', likesCount: 10 },
        { id: v1(), text: 'Ярмарка на манежной площади дала почувствовать новогоднее настроение)', likesCount: 20 },
        { id: v1(), text: 'Кейс: 500.000 $ на разработке 50-ти шаблонов для маркетплейса', likesCount: 980 },
        { id: v1(), text: 'Прямиком из кузницы разработки: аренда яхт в Санкт-Петербурге 💥💥', likesCount: 33 },
        { id: v1(), text: 'Брали интересное интервью и вопросы касательно WordPress', likesCount: 15 }
    ]
}

export type ProfileActionType = ReturnType<typeof addPostActionCreator>
    | ReturnType<typeof updateNewPostTextActionCreator>

const profileReducer = (state: ProfilePageType = initialState, action: ProfileActionType): ProfilePageType => {
    switch (action.type) {
        case 'ADD-POST':
            const newPost: PostType = {
                id: v1(),
                text: action.message,
                likesCount: 0
            };
            return {
                ...state,
                posts: [...state.posts, newPost],
                newPostText: ''
            }
        case 'UPDATE-NEW-POST-TEXT':
            return {
                ...state,
                newPostText: action.text
            }
        default:
            return state;
    }
}

export const addPostActionCreator = (message: string) => ({ type: 'ADD-POST', message } as const)
export const updateNewPostTextActionCreator = (text: string) => ({ type: 'UPDATE-NEW-POST-TEXT', text } as const)

export default profileReducer;