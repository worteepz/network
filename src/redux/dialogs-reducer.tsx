import { v1 } from 'uuid';
import { MessageType, DialogPageType } from './store';

export type DialogsActionType = ReturnType<typeof addDialogMessageActionCreator>
    | ReturnType<typeof updateNewMessageBodyActionCreator>

const initialState: DialogPageType = {
    body: 'Print your message',
    messages: [
        { id: v1(), text: 'Когда же уже будет 100 урок пути Самурая' },
        { id: v1(), text: 'Наверное, скоро' },
        { id: v1(), text: 'Теория маленьких шагов' },
        { id: v1(), text: 'Шаг за шагом' },
        { id: v1(), text: 'Самураи, летим! Шух Вшух Шух' }
    ],
    dialogs: [
        { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
        { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
        { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
        { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
        { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' }
    ]
}

const dialogsReducer = (state: DialogPageType = initialState, action: DialogsActionType): DialogPageType => {

    switch (action.type) {
        case 'ADD-MESSAGE':
            const newDialogMessage: MessageType = {
                id: v1(),
                text: action.dmessage
            }
            return {
                ...state,
                messages: [...state.messages, newDialogMessage]
            }
        case 'UPDATE-NEW-MESSAGE':
            return {
                ...state,
                body: action.body
            }
        default:
            return state;
    }
}

export const addDialogMessageActionCreator = (dmessage: string) =>
    ({ type: 'ADD-MESSAGE', dmessage } as const)

export const updateNewMessageBodyActionCreator = (body: string) =>
    ({ type: 'UPDATE-NEW-MESSAGE', body } as const)

export default dialogsReducer;