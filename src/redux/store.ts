import { UsersPageType, UsersActionType } from './users-reducer';
import { DialogsActionType } from './dialogs-reducer';
import { ProfileActionType } from './profile-reducer';
import { TodolistActionType } from './todolist-reducer';

// Typing Dialogs page

export type DialogType = {
  id: string
  name: string
  surname: string
  image: string
}

export type MessageType = {
  id: string
  text: string
}

export type DialogPageType = {
  body: string
  dialogs: Array<DialogType>
  messages: Array<MessageType>
}

// Typing Profile page

export type PostType = {
  id: string
  text: string
  likesCount: number
}

export type ProfilePageType = {
  description: string
  newPostText: string
  img: string
  posts: Array<PostType>
}

// Typing Todo list page

export type TaskType = {
  id: string
  title: string
  isDone: boolean
}

export type FilterValueType = "all" | "active" | "completed";

export type TodoListType = {
  id: string
  filter: FilterValueType
  name: string
  tasks: Array<TaskType>
}

export type TodoListPageType = {
  todoLists: Array<TodoListType>
}

// Type for redux state

export type ReduxStateType = {
  profilePage: ProfilePageType
  dialogsPage: DialogPageType
  todoListPage: TodoListPageType
  usersPage: UsersPageType
}

export type CommonActionType = DialogsActionType | ProfileActionType | TodolistActionType | UsersActionType 

// type RootStoreType = {
//   _state: RootStateType
//   getState: () => RootStateType
//   _callSubscriber: (state: RootStateType) => void
//   dispatch: (action: DispatchActionType) => void
// }

// const store: RootStoreType = {
//   _state: {
//     profilePage: {
//       description: "Краткое описание моего профиля",
//       img: "https://sun1-25.userapi.com/d2W2baMhX-1rSlU9EMmXl8728m--kaQY4WCvTw/b21rv4H-KEM.jpg",
//       posts: [
//         { id: v1(), text: 'Когда залетел в тусовку к Церебро Таргет,', likesCount: 10 },
//         { id: v1(), text: 'Ярмарка на манежной площади дала почувствовать новогоднее настроение)', likesCount: 20 },
//         { id: v1(), text: 'Кейс: 500.000 $ на разработке 50-ти шаблонов для маркетплейса', likesCount: 98 },
//         { id: v1(), text: 'Прямиком из кузницы разработки: аренда яхт в Санкт-Петербурге 💥💥', likesCount: 33 },
//         { id: v1(), text: 'Брали интересное интервью и вопросы касательно WordPress', likesCount: 15 }
//       ]
//     },
//     dialogsPage: {
//       messages: [
//         { id: v1(), text: 'Когда же уже будет 100 урок пути Самурая' },
//         { id: v1(), text: 'Наверное, скоро' },
//         { id: v1(), text: 'Теория маленьких шагов' },
//         { id: v1(), text: 'Шаг за шагом' },
//         { id: v1(), text: 'Самураи, летим! Шух Вшух Шух' }
//       ],
//       dialogs: [
//         { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
//         { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
//         { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
//         { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' },
//         { id: v1(), name: 'Denis', surname: 'Petrov', image: 'https://clck.ru/Nyjvb' }
//       ]
//     },
//     todoListPage: {
//       todoLists: [
//         {
//           id: v1(),
//           filter: "active",
//           name: "My Todolist",
//           tasks: [
//             { id: v1(), title: "HTML&CSS3", isDone: false },
//             { id: v1(), title: "JS&JQquery", isDone: true },
//             { id: v1(), title: "Java&dotNet", isDone: true }
//           ]
//         },
//         {
//           id: v1(),
//           filter: "completed",
//           name: "Second todolist",
//           tasks: [
//             { id: v1(), title: "HTML&CSS4", isDone: false },
//             { id: v1(), title: "JS&JQqueris", isDone: true },
//             { id: v1(), title: "Ruby&ROr", isDone: true }
//           ]
//         }
//       ]
//     },
//     newsPage: {},
//     sidebar: {}
//   },
//   getState() {
//     return this._state;
//   },
//   _callSubscriber() {
//     console.log('State is changed');
//   },
//   dispatch(action) {
//     profileReducer(this._state.profilePage, action);
//     dialogsReducer(this._state.dialogsPage, action);
//   }
// }