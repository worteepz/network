import { v1 } from "uuid";

type UserLocationType = {
    city: string
    country: string
}

export type UserType = {
    id: string
    photoUrl: string
    followed: boolean
    name: string
    surname: string
    status: string
    location: UserLocationType
}

export type UsersPageType = {
    users: Array<UserType>
}

const initialState = {
    users: [
        { id: v1(), photoUrl: 'https://sun1-25.userapi.com/d2W2baMhX-1rSlU9EMmXl8728m--kaQY4WCvTw/b21rv4H-KEM.jpg', followed: false, name: 'Denis', surname: 'Petrov', status: 'Confectioner', location: { city: 'St. Petersburg', country: 'Russia' } },
        { id: v1(), photoUrl: 'https://sun1-25.userapi.com/d2W2baMhX-1rSlU9EMmXl8728m--kaQY4WCvTw/b21rv4H-KEM.jpg', followed: true, name: 'Julia', surname: 'Menshakova', status: 'Confectioner', location: { city: 'Moscow', country: 'Russia' } },
        { id: v1(), photoUrl: 'https://sun1-25.userapi.com/d2W2baMhX-1rSlU9EMmXl8728m--kaQY4WCvTw/b21rv4H-KEM.jpg', followed: false, name: 'Ivan', surname: 'Petrov', status: 'Confectioner', location: { city: 'St. Petersburg', country: 'Russia' } },
        { id: v1(), photoUrl: 'https://sun1-25.userapi.com/d2W2baMhX-1rSlU9EMmXl8728m--kaQY4WCvTw/b21rv4H-KEM.jpg', followed: false, name: 'Valera', surname: 'Petrov', status: 'Confectioner', location: { city: 'Moscow', country: 'Russia' } },

    ]
}

export type UsersActionType = ReturnType<typeof followAC> | ReturnType<typeof unfollowAC> | ReturnType<typeof setUsersAC>

const usersReducer = (state: UsersPageType = initialState, action: UsersActionType): UsersPageType => {
    switch (action.type) {
        case 'FOLLOW':
            return {
                ...state,
                users: state.users.map(u => {
                    if (u.id === action.userId) {
                        return { ...u, followed: true }
                    }
                    return u;
                })
            }
        case 'UNFOLLOW':
            return {
                ...state,
                users: state.users.map(u => {
                    if (u.id === action.userId) {
                        return { ...u, followed: false }
                    }
                    return u;
                })
            }
        case 'SET-USERS':
            return { ...state, users: [...state.users, ...action.users] } // Склеиваем пользователей, которые были и те, которые пришли
        default:
            return state;
    }
}

export const followAC = (userId: string) => ({ type: 'FOLLOW', userId } as const)
export const unfollowAC = (userId: string) => ({ type: 'UNFOLLOW', userId } as const)
export const setUsersAC = (users: Array<UserType>) => ({ type: 'SET-USERS', users } as const)

export default usersReducer;